require "json"

puts "started "

file = File.open (File.dirname(__FILE__) + '/changelog-neostesia-master.json')
changeLogJson = JSON.load file

IO.write("changelog-neostesia-master.txt", changeLogJson["descriptions"].join("\n"))

file = File.open (File.dirname(__FILE__) + '/changelog-neostesia-staging.json')
changeLogJson = JSON.load file

IO.write("changelog-neostesia-staging.txt", changeLogJson["descriptions"].join("\n"))

puts "ended"

puts changeLogJson["descriptions"]